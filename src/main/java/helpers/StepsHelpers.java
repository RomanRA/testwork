package helpers;

import org.openqa.selenium.WebElement;
import pages.ChooseProductPage;
import pages.MarketSectionPage;
import pages.ProductPage;

import static library.ActiveWebDraiver.initDriver;

public class StepsHelpers {
    private ProductPage productPage = new ProductPage();
    private MarketSectionPage pageMarketSection = new MarketSectionPage();
    private ChooseProductPage chooseProductPage = new ChooseProductPage();

    private void startPage(String url) {
        initDriver.get(url);
    }

    public void selectAdress(String url) {
        switch (url.toLowerCase()) {
            case ("яндекс-маркет"):
                startPage("https://market.yandex.ru/");
                break;
            case ("яндекс"):
                startPage("https://yandex.ru/");
                break;
            default:
                startPage(url);
        }
    }


    public void selectElectronica(String section) {
        pageMarketSection.getSelectElectronics().click();
        switch (section.toLowerCase()) {
            case ("мобильные телефоны"):
                clickElement(pageMarketSection.getSelectMobilePhones());
                break;
            case ("умные часы и браслеты"):
                clickElement(pageMarketSection.getSelectSmartWatchesAndBraslets());
                break;
            default:
                System.out.println("Выбран не сущестующий пункт раздела");
        }
    }

    public void chooseProductName(String productName) {
        switch (productName.toLowerCase()) {
            case ("xiaomi"):
                clickElement(productPage.getChooseProductNameXiaomi());
                break;
            case ("huawei"):
                clickElement(productPage.getChooseProductNameHUAWEI());
                break;
            case ("samsung"):
                clickElement(productPage.getChooseProductNameSamsung());
                break;
            case ("sony"):
                clickElement(productPage.getChooseProductNameSony());
                break;
            default:
                System.err.println("Выберите другойго производителя");
        }
    }

    //    Пишем вид продукта в поисковую строку
    public void setProductNameInSearchLine(String setName) {
        productPage.getSelectProductName().sendKeys(setName);
        productPage.getSelectProductName().submit();
    }

    public void selectSpec() {
        clickElement(chooseProductPage.getSpec());
    }

    public void compareInfoSpecGPU(String nameGpu) {
        assert nameGpu.equals(chooseProductPage.getSpecGPU().getText());

    }

    public void compareInfoChargeType(String nameChargeType) {
        assert nameChargeType.equals(chooseProductPage.getSpecChargeType().getText());

    }

    private void clickElement(WebElement element) {
        element.click();
    }
}
