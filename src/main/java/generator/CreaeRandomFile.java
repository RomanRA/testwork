package generator;

import java.io.*;
import java.util.Random;

/**
 * Создание файла с рандомными числами (int) от 0 до 20 в папку resources
 */
public class CreaeRandomFile {

    public void createRandomNum() throws IOException {

        try (Writer writer = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream("src/main/resources/generator/randomNumbers.txt")
                , "utf-8"))) {
            writer.write(setRandomNumbers());
            writer.close();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    public String setRandomNumbers() {
        String nums = "";
        String ends;
        for (int i = 0; i < 20; i++) {
            if (i < 19) {
                ends = ", ";
            } else {
                ends = "";
            }
            nums += String.valueOf(new Random().nextInt(20)) + ends;
        }
        System.out.println("Создан файл со значениями - " + nums);
        return nums;
    }

}
