package generator;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;

/**
 * Распарсивание файла с рандомными числами и их сортировкой
 * Метод arrSort() сортирует по возрастанию
 * Метод arrReverseSort() сортирует по убыванию
 */
public class ReadRandomFile {

    private String readFile() {
        String content = "";
        try {
            String fileName = "src/main/resources/generator/randomNumbers.txt";

            content = Files.lines(Paths.get(fileName)).reduce("", String::concat);

        } catch (IOException e) {
            System.out.println("Ошибка - " + e.getLocalizedMessage());
        }
        return content;
    }


    public String arrSort() {
        String content = readFile();
        String[] items = content.split(", ");

        int[] results = new int[items.length];

        for (int i = 0; i < items.length; i++) {
            try {
                results[i] = Integer.parseInt(items[i]);
            } catch (NumberFormatException nfe) {
                System.out.println("Error sort -> " + nfe.getLocalizedMessage());
            }
        }
        Arrays.sort(results);
        System.out.println("Отсортирован по возрастанию - " + Arrays.toString(results));
        return Arrays.toString(results);
    }

    public String arrReverseSort() {
        String content = readFile();
        String[] items = content.split(", ");

        Integer[] results = new Integer[items.length];

        for (int i = 0; i < items.length; i++) {
            try {
                results[i] = Integer.parseInt(items[i]);
            } catch (NumberFormatException nfe) {
                System.out.println("Error sort reverse -> " + nfe.getLocalizedMessage());
            }
        }
        Arrays.sort(results, Collections.reverseOrder());
        System.out.println("Отсортирован по убыванию - " + Arrays.toString(results));
        return Arrays.toString(results);
    }
}