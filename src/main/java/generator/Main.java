package generator;

import org.junit.Test;

import java.io.IOException;

public class Main {
    private static CreaeRandomFile creaeRandomFile = new CreaeRandomFile();
    private static ReadRandomFile readRandomFile = new ReadRandomFile();

    //    Выводит информацию в окно
    public static void main(String[] args) throws IOException {
        Window window = new Window();
        window.openWindow();
    }

    //    Выводит информацию в консоль
    @Test
    public void mainTask() throws IOException {
        creaeRandomFile.createRandomNum();
        readRandomFile.arrSort();
        readRandomFile.arrReverseSort();
    }
}
