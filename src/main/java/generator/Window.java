package generator;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;

/**
 * Создает окно с выводом трех строк с данными
 * !!!Если окно ничего не отображает, окно нужно немного растянуть!!!
 */
public class Window {
    private CreaeRandomFile creaeRandomFile = new CreaeRandomFile();
    private ReadRandomFile readRandomFile = new ReadRandomFile();

    public void openWindow() throws IOException {
        JLabel whatCreate, sort, reverseSort;
        JTextField watchWhatCreate, watchSort, watchReverseSort;

        JFrame frame = new JFrame("Растяни меня ))");
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        // Ширина и высота окна
        frame.setSize(850, 200);
        frame.setVisible(true);

        JPanel genXML = new JPanel();
        frame.add(genXML);
        // Задаем разметку окна, количество рядов и строк
        GridLayout gr = new GridLayout(3, 2);

        genXML.setLayout(gr);

        whatCreate = new JLabel("Создан файл со значениями - ");
        genXML.add(whatCreate);
        whatCreate.setHorizontalAlignment(SwingConstants.RIGHT);
        watchWhatCreate = new JTextField();
        genXML.add(watchWhatCreate);
        watchWhatCreate.setText(creaeRandomFile.setRandomNumbers());

        sort = new JLabel("Отсортирован по возрастанию - ");
        genXML.add(sort);
        sort.setHorizontalAlignment(SwingConstants.RIGHT);
        watchSort = new JTextField();
        genXML.add(watchSort);
        watchSort.setText(readRandomFile.arrSort());

        reverseSort = new JLabel("Отсортирован по убыванию - ");
        genXML.add(reverseSort);
        reverseSort.setHorizontalAlignment(SwingConstants.RIGHT);
        watchReverseSort = new JTextField();
        genXML.add(watchReverseSort);
        watchReverseSort.setText(readRandomFile.arrReverseSort());
    }
}
