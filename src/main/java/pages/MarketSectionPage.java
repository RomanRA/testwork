package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static library.ActiveWebDraiver.initDriver;

public class MarketSectionPage {

    @FindBy(css = ".n-w-tab__control-hamburger")
    private WebElement allCategorys;

    @FindBy(xpath = "//span[text()='Электроника']")
    private WebElement selectElectronics;
    @FindBy(xpath = "//span[text()='Компьютерная техника']")
    private WebElement selectComputers;

    @FindBy(xpath = "//a[text()='Мобильные телефоны']")
    private WebElement selectMobilePhones;
    @FindBy(xpath = "//a[text()='Умные часы и браслеты']")
    private WebElement selectSmartWatchesAndBraslets;


    public WebElement getAllCategorys() {
        return allCategorys;
    }

    public WebElement getSelectElectronics() {
        return selectElectronics;
    }

    public WebElement getSelectComputers() {
        return selectComputers;
    }

    public WebElement getSelectMobilePhones() {
        return selectMobilePhones;
    }

    public WebElement getSelectSmartWatchesAndBraslets() {
        return selectSmartWatchesAndBraslets;
    }

    public MarketSectionPage() {
        PageFactory.initElements(initDriver, this);
    }
}
