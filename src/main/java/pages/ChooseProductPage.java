package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static library.ActiveWebDraiver.initDriver;

public class ChooseProductPage {

    @FindBy(xpath = "//li[@data-name='spec']")
    private WebElement spec;

    @FindBy(xpath = "//span[text()='Видеопроцессор']/../following-sibling::dd[@class]")
    private WebElement specGPU;

    @FindBy(xpath = "//span[text()='Тип разъема для зарядки']/../following-sibling::dd[@class]")
    private WebElement specChargeType;

    public WebElement getSpec() {
        return spec;
    }

    public WebElement getSpecGPU() {
        return specGPU;
    }

    public WebElement getSpecChargeType() {
        return specChargeType;
    }

    public ChooseProductPage() {
        PageFactory.initElements(initDriver, this);
    }

}
