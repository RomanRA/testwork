package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static library.ActiveWebDraiver.initDriver;


public class ProductPage {

    @FindBy(xpath = "//input[@id = 'header-search']")
    private WebElement selectProductName;

    @FindBy(xpath = "//span[text()='Xiaomi']")
    private WebElement chooseProductNameXiaomi;
    @FindBy(xpath = "//span[text()='HUAWEI']")
    private WebElement chooseProductNameHUAWEI;
    @FindBy(xpath = "//span[text()='Samsung']")
    private WebElement chooseProductNameSamsung;
    @FindBy(xpath = "//span[text()='Sony']")
    private WebElement chooseProductNameSony;


    public WebElement getChooseProductNameXiaomi() {
        return chooseProductNameXiaomi;
    }

    public WebElement getChooseProductNameHUAWEI() {
        return chooseProductNameHUAWEI;
    }

    public WebElement getChooseProductNameSamsung() {
        return chooseProductNameSamsung;
    }

    public WebElement getChooseProductNameSony() {
        return chooseProductNameSony;
    }

    public WebElement getSelectProductName() {
        return selectProductName;
    }

    public ProductPage() {
        PageFactory.initElements(initDriver, this);
    }
}
