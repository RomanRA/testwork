package library;

import org.openqa.selenium.WebElement;

import java.io.File;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class SaveText {

    public void fileSave(String title, List<WebElement> text) {
        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy-HH-mm-ss");

        String dat = format.format(new Date());

        String separator = File.separator;
        try (FileWriter file = new FileWriter(
                "src" +
                        separator +
                        "testResultsTaskThree" +
                        separator +
                        "-" +
                        dat +
                        ".txt")) {
            file.write(title);
            file.write("\n");
            for (WebElement el : text) {
                file.write(el.getText());
            }
        } catch (Exception e) {
            System.err.println("Всё печально!" + e.getMessage());
        }
    }
}
