package library;

import java.util.ArrayList;

import static library.ActiveWebDraiver.initDriver;

public class SwichesWindowTabs {
    public static void swichTabs() {
        ArrayList<String> tabs2 = new ArrayList<>(initDriver.getWindowHandles());
        initDriver.switchTo().window(tabs2.get(0));
        initDriver.close();
        initDriver.switchTo().window(tabs2.get(1));
    }
}
