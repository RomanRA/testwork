package library;

import org.openqa.selenium.WebDriver;

import static library.webDrivers.InitChromeDriver.initChromeDriver;

public class ActiveWebDraiver {
    public static WebDriver initDriver = initChromeDriver();

    public static void sleepTime(int miliSeconds) {
        try {
            Thread.sleep(miliSeconds * 100);
        } catch (InterruptedException e) {
            System.err.print("Не отработала остановка потока\n");
            e.printStackTrace();
        }
    }
}
