package library.webDrivers;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class InitChromeDriver {
    /**
     * Инициализация CromeDriver
     */

    public static WebDriver initChromeDriver() {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/webDriversFiles/chromedriver(2.40).exe");

        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        return driver;
    }
}
