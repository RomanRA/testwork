package steps;

import cucumber.api.java.ru.Дано;
import cucumber.api.java.ru.И;
import cucumber.api.java.ru.Когда;
import cucumber.api.java.ru.Тогда;
import helpers.StepsHelpers;
import pages.MarketSectionPage;

public class StepMainPageSearchYandex {
    private StepsHelpers stepsHelpers = new StepsHelpers();
    private MarketSectionPage pageMarketSection = new MarketSectionPage();

    @Дано("^совершен переход на страницу \"([^\"]*)\"$")
    public void openURL(String url) {
        stepsHelpers.selectAdress(url);
    }

    @Когда("^выбран раздел электроники \"([^\"]*)\"$")
    public void segment(String section) {
        stepsHelpers.selectElectronica(section);
    }

    @Когда("^товар отфильтрован и выбран по производителю \"([^\"]*)\"$")
    public void selectFiler(String productName) {
        stepsHelpers.setProductNameInSearchLine(productName);
    }

    @Когда("^выбран товар \"([^\"]*)\" из фильтра Производитель$")
    public void chooseProductInFiler(String productName) {
        stepsHelpers.chooseProductName(productName);
    }


    @Когда("^выбран параметр характеристика телефона$")
    public void selectTelepheneSpec() {
        stepsHelpers.selectSpec();
    }

    @Тогда("^у продукта Видеопроцессор \"([^\"]*)\"$")
    public void productGPU(String gpuName) {
        stepsHelpers.compareInfoSpecGPU(gpuName);
    }

    @И("^у продукта Тип разъема для зарядки \"([^\"]*)\"$")
    public void productSpecChargeType(String chargeType) {
        stepsHelpers.compareInfoChargeType(chargeType);

    }
}
