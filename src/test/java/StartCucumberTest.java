import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;
import org.junit.AfterClass;
import org.junit.runner.RunWith;

import static library.ActiveWebDraiver.initDriver;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/features",
        glue = "steps",
        tags = "@findXiaomi_Mi6_6/64",
        snippets = SnippetType.CAMELCASE
)
public class StartCucumberTest {
    @AfterClass
    public static void closeWindow() {
        initDriver.quit();
    }
}
